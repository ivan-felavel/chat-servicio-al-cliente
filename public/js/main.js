var audio = new Audio('poke.mp3');

$("#inicio").on("click", function(){
	$('.home').siblings().hide();
	$('.home').show();
});


$("#conversaciones").on("click", function(){
	$('.conversaciones').siblings().hide();
	$('.conversaciones').show();
});


$("#admin").on("click", function(){
	$('.adminOps').siblings().hide();
	$('.adminOps').show();
});

$(".clienteConectado").on("click", function(event) {
	event.preventDefault();
	var idCliente = "chatCliente" + $(this).attr("id").substr(7,);
	$(this).children(".msgLabel").hide()
	if ($("#" + idCliente).length) {
		console.log("conectado");
		$('.fullscreen.modal#' + idCliente).modal('show');
	}
	else {
		var nombreCliente = $(this).children(".header").text();
		console.log(nombreCliente);
		var cajaDeChat = '<div class="ui fullscreen modal transition hidden" id="' + idCliente + '" style="margin-top: -212.769px;"> <i class="close icon"></i> <div class="header">' + nombreCliente + '</div><div class="messages" style="background: white; height: 250px; font-size: 12px; padding: 15px; overflow:auto; overflow-x: hidden;"> <div class="msg_push_old"></div><div class="msg_push_new"></div></div><div class="typing"> </div><div class="ui form"> <div class="field"> <textarea rows="1" class="inputMessage" name="msg" placeholder="Escriba aquí"></textarea> </div></div></div>'	
		$(".conversaciones").append(cajaDeChat);
		$('.fullscreen.modal#' + idCliente).modal('show');

	}
})

$(document).on("keydown", ".inputMessage", function(event) {
	$inputMessage = $(this);
	$messages = $inputMessage.parents(".ui.form").siblings(".messages");
	var $newMsg = $messages.children('.msg_push_new');
	var $oldMsg = $messages.children('.msg_push_old');

	if (event.which !== 13) {
	console.log("typing...");
	}
	else {
	// TO-DO Enviar mensaje junto con id del socket y nombre de usuario
	event.preventDefault();
	var username = "Alguien";
	var msg = $(this).val();
	var time = new Date().toString();
	if (msg) {
	    $(this).val("");
	    var $messageBodyDiv = $('<div class="ui blue message"> <div class="header">' + username + '</div> <div class="msgContent">' + msg + '</div> <div class="dateOfMessage">' + (time.toLocaleString().substr(15, 6)) + '</div></div>').insertBefore($newMsg);
	    $messages[0].scrollTop = $messages[0].scrollHeight;
	}
	}
});

function msgNotification(id) {

	var idClienteConectado = "cliente" +  id.substr(11,);
	console.log(idClienteConectado);
	if ($("#" + id).hasClass("hidden")) {
		$("#" + idClienteConectado).children(".msgLabel").show();
		audio.play();
		$("#" + idClienteConectado).children(".msgLabel").children(".notificationIcon").transition("bounce");
	} 
	addReceivedMsg("Alguien", "mensaje de respuesta", id);
}

function addReceivedMsg(username, msg, id) {
	var $messages = $("#" + id).children(".messages");
  	var $newMsg = $messages.children('.msg_push_new');
	var $oldMsg = $messages.children('.msg_push_old');
    var time = new Date().toString();
    if (msg) {
    	var $messageBodyDiv = $('<div class="ui green message"> <div class="header">' + username + '</div> <div class="msgContent">' + msg + '</div> <div class="dateOfMessage">' + (time.toLocaleString().substr(15, 6)) + '</div></div>').insertBefore($newMsg);
    	$messages[0].scrollTop = $messages[0].scrollHeight;
	}
}

function isTyping(id, username) {
	var $typing = $("#" + id).children(".typing");
	$typing.text(username + " está escribiendo");
}

function isNotTyping(id) {
	var $typing = $("#" + id).children(".typing");
	$typing.text("");
}